
import cv2
import numpy as np
import sys


# Print iterations progress Print iterations progress Print iterations progress
#Options d'affichages progress bar
WITH_PROGRESS_BAR=True
PGSBAR_KEEP_ON_COMPLETE=False
#Options d'affichages image
WITH_RT_FRAME=True
#Bypass des calculs
WITHOUT_CALCUL=False


# Print iterations progress
# Print iterations progress
if WITH_PROGRESS_BAR:
    def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
        # Print New Line on Complete
        if iteration == total:
            if PGSBAR_KEEP_ON_COMPLETE :
                print()
            else:
                toClear = length + len(suffix) + len(prefix) + 7 + (decimals + 3)
                print("\r" + " "*(toClear))
else:
    def printProgressBar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
        pass

#not used anymore
def mightRepresentVCapid(s : str):
    try:
        if int(s)>=0 :
            return True
        return False
    except ValueError:
        return False

#luminance function taken from : https://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
def luminance(p : np.array):
    #(0.2126*R + 0.7152*G + 0.0722*B)
    #TODO check opti
    return 0.2126*p[2] + 0.7152*p[1] + 0.0722*p[0]

#Need at least the input source
if(len(sys.argv) < 2):
    print("Sorry, need arg")
    exit(1)

#if vcap id, translate to int
numberOfFrames = 0
inputArg = sys.argv[1]
if sys.argv[1].isdigit() :
    inputArg = int(sys.argv[1])
    numberOfFrames = -1 # flag

#open cap
cap = cv2.VideoCapture(inputArg)

#get number of frames
if numberOfFrames != -1 :
    numberOfFrames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

## First frame !

# Capture frame-by-frame
ret, frame = cap.read()

#Get frame height and width to access pixels
height, width, channels = frame.shape

# frames passed
numberOfFramesPassed = 0
# frames passed reduced to 1024
numberOfFramesReduced = 0
reducedTime = 1024
##TODO period ...

# keep video reduced
data = np.zeros((width, height, reducedTime), dtype=np.float32)
#np.array 3dim type : (int8, int8, int8)
#                   : luminance int8
#np.array[x,y] => np.array([l], [l], [l], ...)

#DBG info
#print size et ...
print(f"size of capture is {width}x{height}")
while(True):


    # Display the resulting frame / Show progression
    if WITH_RT_FRAME :
        cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    #Accessing BGR pixel value
    if not WITHOUT_CALCUL :
        for y in range(0, height) :
            for x in range(0, width) :
                val = luminance(frame[y,x])
                data[x, y, numberOfFramesPassed] = val

    # Number of frames passed ++
    numberOfFramesPassed += 1

    # Display numberOfFrames
    if WITH_PROGRESS_BAR :
        if numberOfFrames == -1 :
            print('\r', numberOfFramesPassed, end='\r')
        else : #Or the progress bar
           printProgressBar(numberOfFramesPassed, numberOfFrames, prefix = 'Progress:', suffix = 'Complete (' + str(numberOfFramesPassed) + ')', length = 50)

    # Capture frame-by-frame
    ret, frame = cap.read()

    # No more frames
    if(ret == False):
        break


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

