#!/bin/bash

FFMPEG="ffmpeg.exe"


echo "usage :"
echo "./cut.sh cutFFT <startTime> <source> <dest>"


if [[ $1 == "cutFFT" ]] ; then
	if [[ $# == 4 ]] ; then
		${FFMPEG} -ss $2 -i $3 -c:v libx264 -c:a aac -frames:v 1024 $4
	else
		echo "not enough arg for cutFFT command"
	fi
fi

